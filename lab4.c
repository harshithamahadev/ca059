#include <stdio.h>
int main()
{
    int a;
    printf("Enter the number from 1-12\n");
    scanf("%d",&a);
    switch(a)
    {
        case 1:
            printf("The month corresponding to this number is January");
            break;
        case 2:
            printf("The month corresponding to this number is February");
            break;
        case 3:
            printf("The month corresponding to this number is March");
            break;
        case 4:
            printf("The month corresponding to this number is April");
            break;
        case 5:
            printf("The month corresponding to this number is May");
            break;
        case 6:
            printf("The month corresponding to this number is June");
            break;
        case 7:
            printf("The month corresponding to this number is July");
            break;
        case 8:
            printf("The month corresponding to this number is August");
            break;
        case 9:
            printf("The month corresponding to this number is September");
            break;
        case 10:
            printf("The month corresponding to this number is October");
            break;
        case 11:
            printf("The month corresponding to this number is November");
            break;
        case 12:
            printf("The month corresponding to this number is December");
            break;
        default:
            printf("Invalid choice");
    }
return 0;
}
