#include<stdio.h>
void swap(int *, int *);
int main()
{
    int a,b,*x,*y;
    x=&a;
    y=&b;
    printf("Enter the two numbers\n");
    scanf("%d%d",x,y);
    printf("The numbers before swapping are %d and %d\n",*x,*y);
    swap(x,y);
    return 0;
}
void swap(int *x,int *y)
{
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
    printf("The numbers after swapping are %d and %d\n",*x,*y);
}
