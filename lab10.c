#include<stdio.h>
int main()
{
    int a,b,*x,*y;
    x=&a;
    y=&b;
    printf("Enter the two numbers\n");
    scanf("%d%d",x,y);
    printf("The sum of the two numbers is: %d\n",*x+*y);
    printf("The difference between the two numbers is: %d\n",*x-*y);
    printf("The product of the two numbers is: %d\n",(*x)*(*y));
    printf("The quotient is: %d\n",(*x)/(*y));
    return 0;
}
