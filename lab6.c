#include<stdio.h>
float average(int a[50],int n)
{
    int i;
    float sum=0.0,avg;
    for(i=0;i<n;i++)
        sum=sum+a[i];
    avg=sum/n;
    return avg;
}
void display()
{
    float avg;
    int a[50],n,i;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    printf("Enter the elements\n");
    for(i=0;i<n;i++)
    {
        printf("a[%d]=",i);
        scanf("%d",&a[i]);
    }
    avg=average(a,n);
    printf("the average of the elements of the array is %0.2f\n",avg);
}

int main()
{
    display();

    return 0;
}
