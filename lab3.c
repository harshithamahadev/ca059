#include<math.h>
#include<stdio.h>
void quad(int a,int b,int c)
{
    float d,s1,s2,s3;
    d=b*b-4*a*c;
    if(d>0)
    {
        printf("The roots are real and unique\n");
        s1=(-b+sqrt(d))/2*a;
        s2=(-b-sqrt(d))/2*a;
        printf("the solutions are=%f,%f\n",s1,s2);
    }
    else if(d==0)
    {
        printf("The roots are real and equal\n");
        s1=(-b+sqrt(d))/2*a;
        s2=s1;
        printf("the solutions are=%f,%f\n",s1,s2);
    }
    else
    {
        printf("The roots are imaginary\n");
    }
}
int main()
{
    int a,b,c;
    float s1,s2;
    printf("Enter the coefficient of x^2\n");
    scanf("%d",&a);
    printf("Enter the coefficient of x\n");
    scanf("%d",&b);
    printf("Enter the numeral part\n");
    scanf("%d",&c);
    quad(a,b,c);
    return 0;
}
